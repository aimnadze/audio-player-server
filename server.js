function parsePost (req, callback) {
    var postData = ''
    req.on('data', function (data) {
        postData += data
    })
    req.on('end', function () {
        var params = querystring.parse(postData)
        callback(params)
    })
}

var config = require('./config.js'),
    child_process = require('child_process'),
    crypto = require('crypto'),
    http = require('http'),
    querystring = require('querystring'),
    url = require('url'),
    AudioFile = require('./classes/AudioFile.js'),
    Player = require('./classes/Player.js')

var testMplayer = child_process.spawn('mplayer', ['--version'])
testMplayer.on('error', function (err) {
    if (err.code == 'ENOENT') {
        console.error('ERROR: "mplayer" command not found.')
        process.exit(1)
    }
})
testMplayer.on('exit', function () {

    function sendJson (res, json) {
        res.setHeader('Content-Type', 'application/json')
        res.end(JSON.stringify(json))
    }

    var player = Player()

    http.createServer(function (req, res) {
        parsePost(req, function (params) {
            var parsedUrl = url.parse(req.url, true),
                keyParam = params.key || ''
            if (crypto.createHash('sha1').update(keyParam).digest('hex') == config.keySha1) {
                var pathname = parsedUrl.pathname
                if (pathname == '/enqueue') {
                    var filename = params.filename || '',
                        title = params.title || '',
                        audioFile = AudioFile(filename, title)
                    player.enqueue(audioFile)
                    sendJson(res, { id: audioFile.id })
                } else if (pathname == '/pause') {
                    player.pause()
                    sendJson(res, {})
                } else if (pathname == '/play') {
                    player.play()
                    sendJson(res, {})
                } else if (pathname == '/removeAudioFile') {
                    player.removeAudioFile(params.id)
                    sendJson(res, {})
                } else if (pathname == '/state') {
                    sendJson(res, player.getState())
                } else if (pathname == '/wait-state') {
                    ;(function () {

                        function sendState () {
                            res.end(JSON.stringify(player.getState()))
                        }

                        var sequence = parseInt(params.sequence, 10)
                        if (sequence != player.getStateSequence()) {
                            sendState()
                        } else {

                            function stateChange () {
                                clearTimeout(timeout)
                                sendState()
                            }

                            player.onceStateChange(stateChange)

                            var timeout = setTimeout(function () {
                                res.end('null')
                            }, 30000)

                        }

                    })()
                } else if (pathname == '/volumeUp') {
                    player.volumeUp()
                    sendJson(res, {})
                } else if (pathname == '/volumeDown') {
                    player.volumeDown()
                    sendJson(res, {})
                } else {
                    res.statusCode = 404
                    res.end('404 Not Found')
                }
            } else {
                res.statusCode = 403
                res.end('403 Forbidden')
            }
        })
    }).listen(config.port, config.host)

})
