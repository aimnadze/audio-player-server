Audio Player Server
===================
Audio Player Server ("server") is an audio player program without a GUI but with HTTP API to control it.
Its features include:
* Ability to play audio files from the local file system as well as from remote URLs.
* Ability to create a queue of audio files.
* Ability to pause/resume playing.
* Ability to volume up/down the player.
* Integrated search for audio files from OGG.GE.
* Player security with a key.

The server requires GNU/Linux operating system, Node.js and mplayer2 to run.

API Methods
-----------
The URL of the `doSomething` method is `http://127.0.0.1:7075/doSomething`.
The methods should be called with HTTP POST method and the parameters
should be sent as a urlencoded form data. If the server is secured with
a key all the methods will require the `key` parameters.
Below is the list of methods currently supported by the Server:

* `enqueue` - Adds an audio file to the playlist.

  POST Parameters:
  * `filename` - The location of the audio file.
  * `title` - The title for displaying purposes.

  JSON response:
```
    {
      id: <id>, // An identifier of the newly enqueued audio file.
    }
```

* `getAudioFiles` - Returns the list of audio files from the playlist.

  JSON response:
```
    {
      audioFiles: [
        {
          id: <id>,
          filename: <filename>,
          title: <title>,
        },
        ...
      ],
    }
```
* `pause` - Pauses playing of the playlist.
* `play` - Resumes playing of the playlist.
* `removeAudioFile` - Removes an audio file from the playlist.

  POST Parameters:
  * `id` - The identifier of the audio file to remove.
* `state` - Returns the state of the player.

  JSON response:
```
    {
      paused: <paused>, // true when the player is paused, otherwise false
    }
```
* `volumeUp` - Volume up the player.
* `volumeDown` - Volume down the player.

Scripts
-------
* `./restart.sh` - Start/restart the server.
* `./stop.sh` - Stop the server.
* `./scripts/randomize-key.js` - Generate a random key. Restart the server to make it effective.
* `./scripts/disable-key.js` - Clear the key. Restart the server to make it effective.

See Also
--------
* [Remote Player](https://github.com/archilimnadze/remote-player/) - A PHP frontend for the server.
