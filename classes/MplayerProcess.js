function MplayerProcess (filename, initialVolume) {

    function onExit (code) {
        ended = true
        if (code) errorListener()
        else endListener()
    }

    var worker = child_process.spawn('mplayer', [
        '-quiet',
        '-cache', '2048',
        '-volume', initialVolume,
        filename,
    ])
    worker.stdout.pipe(process.stdout)
    worker.stderr.pipe(process.stderr)
    worker.on('exit', onExit)

    var stdin = worker.stdin,
        ended = false,
        paused = false

    var endListener,
        errorListener

    return {
        isPaused: function () {
            return paused
        },
        kill: function () {
            worker.removeListener('exit', onExit)
            worker.kill('SIGKILL')
        },
        onEnd: function (listener) {
            endListener = listener
        },
        onError: function (listener) {
            errorListener = listener
        },
        pause: function () {
            if (!paused && !ended) {
                paused = true
                stdin.write(' ')
            }
        },
        play: function () {
            if (paused && !ended) {
                paused = false
                stdin.write(' ')
            }
        },
        volumeDown: function () {
            if (!ended) stdin.write('9')
        },
        volumeUp: function () {
            if (!ended) stdin.write('0')
        },
    }

}

var child_process = require('child_process')

module.exports = MplayerProcess
