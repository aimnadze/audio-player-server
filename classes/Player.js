function Player () {

    function emitStateChange () {
        stateSequence++
        while (stateChangeListeners.length) {
            stateChangeListeners.shift()()
        }
    }

    function playFirstItem () {
        if (audioFiles.length) {
            mplayerProcess = MplayerProcess(audioFiles[0].filename, volume)
            mplayerProcess.onError(function () {
                playFirstItem()
                emitStateChange()
            })
            mplayerProcess.onEnd(function () {
                audioFiles.shift()
                playFirstItem()
                emitStateChange()
            })
        } else {
            mplayerProcess = null
        }
    }

    var audioFiles = []

    var mplayerProcess = null

    var paused = false

    var volume = 81

    var stateSequence = 0
    var stateChangeListeners = []

    return {
        enqueue: function (audioFile) {
            audioFiles.push(audioFile)
            if (!paused && !mplayerProcess) playFirstItem()
            emitStateChange()
        },
        getStateSequence: function () {
            return stateSequence
        },
        getState: function () {
            return {
                sequence: stateSequence,
                paused: paused,
                volume: volume,
                audioFiles: audioFiles,
            }
        },
        onceStateChange: function (listener) {
            stateChangeListeners.push(listener)
        },
        pause: function () {
            paused = true
            if (mplayerProcess) mplayerProcess.pause()
            emitStateChange()
        },
        play: function () {
            paused = false
            if (mplayerProcess) mplayerProcess.play()
            else playFirstItem()
            emitStateChange()
        },
        removeAudioFile: function (id) {
            for (var i = 0; i < audioFiles.length; i++) {
                if (audioFiles[i].id == id) {
                    audioFiles.splice(i, 1)
                    if (!i) {
                        var wasPlaying = false
                        if (mplayerProcess) {
                            wasPlaying = !mplayerProcess.isPaused()
                            mplayerProcess.kill()
                            mplayerProcess = null
                        }
                        if (wasPlaying) playFirstItem()
                    }
                    emitStateChange()
                    break
                }
            }
        },
        unStateChange: function (listener) {
            stateChangeListeners.splice(stateChangeListeners.indexOf(listener), 1)
        },
        volumeDown: function () {
            if (volume > 0) {
                volume = Math.max(0, volume - 3)
                if (mplayerProcess) mplayerProcess.volumeDown()
                emitStateChange()
            }
        },
        volumeUp: function () {
            if (volume < 100) {
                volume = Math.min(100, volume + 3)
                if (mplayerProcess) mplayerProcess.volumeUp()
                emitStateChange()
            }
        },
    }

}

var MplayerProcess = require('./MplayerProcess.js')

module.exports = Player
