#!/usr/bin/env node

process.chdir(__dirname)

var crypto = require('crypto'),
    fs = require('fs'),
    config = require('../config.js')

var newPasswordSha1 = crypto.createHash('sha1').update('').digest('hex')

var content =
    '// auto-generated\n' +
    'exports.port = ' + config.port + '\n' +
    'exports.host = ' + JSON.stringify(config.host) + '\n' +
    'exports.keySha1 = ' + JSON.stringify(newPasswordSha1) + '\n'

fs.writeFileSync('../config.js', content)

console.log('New key is an empty string')
